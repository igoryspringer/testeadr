-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июл 24 2020 г., 15:16
-- Версия сервера: 5.7.30-0ubuntu0.18.04.1
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testEADR`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ATMachines`
--

CREATE TABLE `ATMachines` (
  `id` int(6) NOT NULL,
  `title` varchar(11) CHARACTER SET utf8mb4 NOT NULL,
  `date` datetime NOT NULL,
  `sum` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `ATMachines`
--

INSERT INTO `ATMachines` (`id`, `title`, `date`, `sum`) VALUES
(43, 'АТМ1', '2010-02-20 01:00:00', 121000),
(44, 'АТМ2', '2010-02-20 01:05:00', 40100),
(45, 'АТМ3', '2010-02-20 01:05:00', 400000),
(46, 'АТМ1', '2010-02-20 02:00:00', 120000),
(47, 'АТМ2', '2010-02-20 02:10:00', 39200),
(48, 'АТМ1', '2010-02-20 03:00:00', 118200),
(49, 'АТМ2', '2010-02-20 03:15:00', 33600),
(50, 'АТМ3', '2010-02-20 03:20:00', 350000),
(51, 'АТМ1', '2010-02-20 04:00:00', 115400),
(52, 'АТМ2', '2010-02-20 04:15:00', 30200),
(53, 'АТМ3', '2010-02-20 04:20:00', 300000),
(54, 'АТМ1', '2010-02-20 05:00:00', 110000),
(55, 'АТМ2', '2010-02-20 05:20:00', 25400),
(56, 'АТМ1', '2010-02-20 06:00:00', 300000);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ATMachines`
--
ALTER TABLE `ATMachines`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ATMachines`
--
ALTER TABLE `ATMachines`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
